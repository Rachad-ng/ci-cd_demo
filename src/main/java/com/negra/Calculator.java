package com.negra;

public class Calculator {

    public int calculateEcart( int a, int b ) {
        if ( a > b )
            return a - b;
        else if ( b > a )
            return b - a;
        else
            return 0;
    }

    public int calculateSum( int[] tab ) {
        int sum = 0;
        for ( int x : tab )
            sum += x;

        return sum;
    }

}
