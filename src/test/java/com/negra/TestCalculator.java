package com.negra;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestCalculator {

    Calculator cal;
    int[]      tab;

    @Before
    public void setUp() {
        cal = new Calculator();
        tab = new int[] { 10, 5, 12, 12 };
    }

    @Test
    public void testCalculateEcart() {
        assertEquals( 5, cal.calculateEcart( tab[0], tab[1] ) );
        assertEquals( 2, cal.calculateEcart( tab[0], tab[2] ) );
        assertEquals( 0, cal.calculateEcart( tab[2], tab[3] ) );
    }

    @Test
    public void testCalculateSum() {
        assertEquals( 39, cal.calculateSum( tab ) );
    }

    @After
    public void tearDown() {
        cal = null;
        tab = null;
    }
}
